let slider = document.getElementById('slider');

chrome.storage.sync.get('toggleOrder', ({ toggleOrder }) => {
    slider.checked = toggleOrder;
});

slider.addEventListener('change', () => {
    chrome.storage.sync.set({ toggleOrder: slider.checked });
});