// Wählen Sie das Ziel-Element aus
const targetNode = document.body;

// Optionen für den Observer (welche Veränderungen beobachtet werden sollen)
const config = { attributes: true, childList: true, subtree: true };

// Callback-Funktion bei Änderungen
const callback = function (mutationsList, observer) {
    for (const mutation of mutationsList) {
        if (mutation.type === 'childList') {
            for (const node of mutation.addedNodes) {
                if (node.classList && node.classList.contains('emojis-container')) {
                    const emojiPicker = document.querySelector('.emoji-picker');
                    if (emojiPicker) {
                        //setzt die Höhe des Emoji Fensters auf 500px
                        emojiPicker.style.height = '500px';
                        //setzt die Höhe des Emoji Fensters auf 450px
                        emojiPicker.style.maxWidth = "450px";

                        //Setzt die RBTV Emotes an erster Stelle
                        chrome.storage.sync.get('toggleOrder', ({ toggleOrder }) => {
                            if (toggleOrder) {
                                //RBTV Emotes nach oben schieben
                                var custom_list = document.querySelectorAll('[data-section="custom-default"]');
                                var custom_array = [...custom_list]; // converts NodeList to Array
                                custom_array.forEach(custom => {
                                    var parent = custom.parentNode;
                                    parent.insertBefore(custom, parent.firstChild);
                                });

                                //Zuletzt verwendete Emotes nach oben schieben
                                var recent_list = document.querySelectorAll('[data-section="recent"]');
                                var recent_array = [...recent_list]; // converts NodeList to Array
                                recent_array.forEach(recent => {
                                    var parent = recent.parentNode;
                                    parent.insertBefore(recent, parent.firstChild);
                                });
                            }
                        });

                    }
                }
            }
        }
    }
};

// Erstellen Sie einen Observer-Instanz mit der Callback-Funktion
const observer = new MutationObserver(callback);

// Starten Sie den Observer mit den Optionen
observer.observe(targetNode, config);