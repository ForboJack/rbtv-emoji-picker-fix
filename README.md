Ich hab für mich eine Browser Erweiterung gebastelt um das Emoji Fenster im RBTV Forum in der Höhe anzupassen.

Wenn ihr die Erweiterung auch nutzen wollt, könnt ihr sie hier runterladen.

Dies ist keine offizielle Erweiterung. Dafür bräuchte ich einen Entwickleraccount und der kostet Geld. Ich übernehme keine Haftung für diese App. Nutzt sie auf eigene Gefahr. Es sind aber nur ein paar Zeilen Javascript, die ihr selber einsehen und verändern könnt.

Für Firefox findet ihr eine kompilierte Version im Release Ordner.

Wenn ihr nicht wisst, wie ihr unter Chrome Erweiterungen selber installiert:
 
    1. Ruft chrome://extensions auf.

    2. Aktiviert rechts oben den Entwicklermodus.

    3. Klickt auf "Entpackte Erweiterung laden".

    4. Sucht den Ordner für die App/Erweiterung und wählt diesen aus.

Um das Addon unter FireFox debuggen zu können, muss die "manifest_gecko.json" in "manifest.json" umbenannt werden.